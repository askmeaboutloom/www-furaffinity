BEGIN { do 't/testbase.pl' }


$fa = WWW::FurAffinity->new(name => 'user');

httpd { res_file('index_guest') };
throws_ok { $fa->scrape->messages } qr/not logged in/,
          'scrape messages without logging in dies';


httpd { res_file('no_messages') };
cmp_deeply $fa->scrape->messages, {
               submissions => 0,
               comments    => 0,
               journals    => 0,
               favorites   => 0,
               watches     => 0,
               notes       => 0,
               tickets     => 0,
               total       => 0,
           }, 'scraping no messages returns all zeros';


httpd { res_file('no_messages') };
cmp_deeply $fa->scrape->messages, {
               submissions => 0,
               comments    => 0,
               journals    => 0,
               favorites   => 0,
               watches     => 0,
               notes       => 0,
               tickets     => 0,
               total       => 0,
           }, 'scraping no messages';


httpd { res_file('23C_61F_124W') };
cmp_deeply $fa->scrape->messages, {
               submissions => 0,
               comments    => 23,
               journals    => 0,
               favorites   => 61,
               watches     => 124,
               notes       => 0,
               tickets     => 0,
               total       => 208,
           }, 'scraping comments, favorites and watches';


{
    no warnings 'redefine';
    my $destroyed;
    local *WWW::FurAffinity::DESTROY = sub { $destroyed = 1 };
    $fa = undef;
    ok $destroyed, 'weak reference to parent works';
}


done
