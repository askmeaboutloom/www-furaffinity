use Test::Most;
use autodie;
use diagnostics;
use Mojo::Message::Request;
use Test::Fake::HTTPD;
use Time::Piece;
use WWW::FurAffinity;


our ($fa, $httpd);


sub httpd(&)
{
    my ($code) = @_;
    $httpd = undef;
    $httpd = run_http_server
    {
        local $_ = Mojo::Message::Request->new();
        $_->parse(shift->as_string);
        $code->();
    };
    $WWW::FurAffinity::fa = $httpd->endpoint;
}


sub res_file
{
    my $file = shift;
    my $code = shift || 200;
    open my $fh, '<', "t/html/$file.html";
    [$code, ['Content-Type' => 'text/html'], $fh]
}


sub redirect
{
    my $location = shift;
    my $code     = shift || 303;
    [$code, [Location => $location], []]
}


sub iso_timepiece
{   Time::Piece->strptime(@_, '%Y-%m-%dT%H:%M:%S') }


sub done
{
    delete $fa->{logged_in} if defined $fa;
    $httpd = $fa = undef;
    done_testing
}


1;
