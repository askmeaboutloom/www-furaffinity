BEGIN { do 't/testbase.pl' }


my $logged_in;
httpd
{
    if ($_->method eq 'GET')
    {
        if ($_->url eq '/')
        {
            return res_file($logged_in ? 'index' : 'index_guest');
        }
        elsif ($_->url =~ /login/)
        {
            return res_file('login');
        }
        elsif ($_->url =~ /logout/)
        {
            $logged_in = 0;
            return res_file('index_guest');
        }
    }
    elsif ($_->method eq 'POST')
    {
        if ($_->param('name') eq 'user' && $_->param('pass') eq 'pass')
        {
            $logged_in = 1;
            return res_file('index');
        }
        else
        {
            return res_file('bad_login');
        }
    }
    res_file('404', 404)
};


$fa = WWW::FurAffinity->new(name => 'user');
ok !$fa->logged_in, 'constructing with no password does not log in';

throws_ok { $fa->log_in('wrong') } qr/Couldn't log in/, 'wrong password dies';
ok !$fa->logged_in, 'wrong password does not log in';

$fa->log_in('pass');
ok $fa->logged_in, 'correct password logs in';

$fa->log_out;
ok !$fa->logged_in, 'logging out works correctly';


$fa = WWW::FurAffinity->new(name => 'user', password => 'pass');
ok $fa->logged_in, 'constructing with password logs in';

dies_ok { $fa->get('nonexistent') } 'GETting bad response dies';
like $fa->dom->at('title')->text, qr/Error 404/, 'dom returns bad response';

$fa = undef;
ok !$logged_in, 'destructing while logged in logs out automatically';


throws_ok { WWW::FurAffinity->new(name => 'wrong', password => 'pass') }
          qr/Couldn't log in/, 'wrong user dies';


$fa = WWW::FurAffinity->new({name => 'user', password => 'pass'});
ok $fa->logged_in, 'logging in again';

httpd { res_file(404) };
$fa->get;
throws_ok { $fa->logged_in } qr/Can't tell if user is logged in or not/,
          'not being able to tell login status dies';

httpd { res_file('index') };
throws_ok { $fa->log_out } qr/Couldn't log out/, 'failing to log out dies';


done
