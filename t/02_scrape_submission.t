BEGIN { do 't/testbase.pl' }


httpd
{
    if ($_->url =~ m{^/(?:view|full)/(\d+)} && -e "t/html/submission$1.html")
    {
        return res_file("submission$1");
    }
    res_file('404', 404)
};

$fa = WWW::FurAffinity->new(name => 'user');


sub submission_ok
{
    my ($id, $want, $name) = @_;

    for ('', "$WWW::FurAffinity::fa/view/", "$WWW::FurAffinity::fa/full/",
            ["$WWW::FurAffinity::fa/view/", '/?nocache=123456789'])
    {
        my ($prefix, $suffix) = ref $_ ? @$_ : ($_, '');
        my  $url              = "$prefix$id$suffix";
        cmp_deeply $fa->scrape->submission($url), $want, $name;
    }
}


submission_ok 13648607, {
    title       => 'War stream No!',
    artist      => 'HierroTatsu',
    artist_link => '/user/hierrotatsu/',
    date        => iso_timepiece('2014-06-05T14:43:00'),
    keywords    => [],
    category    => 'All',
    theme       => 'All',
    species     => 'Unspecified / Any',
    gender      => 'Any',
    favorites   => 7,
    comments    => 1,
    views       => 47,
    resolution  => '1280x1280',
    rating      => 'general',
    download    => '//d.facdn.net/art/hierrotatsu/1401993790/'
                 . '1401993790.hierrotatsu_140601_tatsuwarstream.png',
}, 'scrape artwork';


submission_ok 12166426, {
    title       => 'Chiptuney Thing That Actually Works',
    artist      => 'askmeaboutloom',
    artist_link => '/user/askmeaboutloom/',
    date        => iso_timepiece('2013-11-27T16:13:00'),
    keywords    => [qw(Music Chiptune Calm Mellow Melancholic MIDI Song)],
    category    => 'Music',
    theme       => 'Game Music',
    favorites   => 1,
    comments    => 13,
    views       => 91,
    rating      => 'general',
    download    => '//d.facdn.net/art/askmeaboutloom/music/1385586788/'
                 . '1385586788.askmeaboutloom_vhk.mp3',
}, 'scrape music';


# TODO more tests


done
