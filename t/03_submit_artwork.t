BEGIN { do 't/testbase.pl' }

my %args = (
    file   => 'logo.png',
    title  => 'logo',
    rating => 'general',
);


$fa = WWW::FurAffinity->new(name => 'user');

httpd { res_file('index_guest') };
throws_ok { $fa->submit->artwork(%args) } qr/not logged in/,
          'submit artwork without logging in dies';


my $next = 12344;
my %got;
httpd
{
    if ($_->method eq 'GET')
    {
        if ($_->url eq '/submit/')
        {
            return res_file('submit');
        }
        elsif ($_->url =~ m{^/view/\d+$})
        {
            return [200, ['Content-Type' => 'text/plain'],
                         [map { "$_ $got{$_}\n" } keys %got]];
        }
    }
    elsif ($_->method eq 'POST')
    {
        my $r = $_;
        if ($_->param('part') == 2)
        {
            return res_file('upload_artwork');
        }
        elsif ($_->param('part') == 3)
        {
            return res_file('finalize_artwork');
        }
        elsif ($_->param('part') == 5)
        {
            %got = map { $_ => $r->param($_) } qw(title message keywords rating
                                                scrap cat atype species gender);
            return redirect($_->param('keywords') eq 'fail'
                            ? '/submit/' : '/view/' . ++$next);
        }
    }
    res_file('404', 404)
};


dies_ok { $fa->submit->artwork({}) } 'missing all arguments dies';

for (qw(file title rating))
{
    my %wrong = %args;
    delete $wrong{$_};
    throws_ok { $fa->submit->artwork(%wrong) }
              qr/missing required argument '$_'/, "missing $_ dies";
}

for (qw(file thumb))
{
    throws_ok { $fa->submit->artwork(%args, $_ => 'nonexistent') }
              qr/not a file: 'nonexistent'/, "nonexistent $_ dies";
    throws_ok { $fa->submit->artwork(%args, $_ => 'lib') }
              qr/not a file: 'lib'/,     "using folder as $_ dies";
}


sub got_ok
{
    my %res = map { chomp; split ' ', $_, 2 } split /^/, $fa->{mech}->content;
    cmp_deeply \%res, shift, '...correct parameters received';
}

is $fa->submit->artwork(%args), 12345, 'minimal submission';
got_ok {
    title    => 'logo',
    message  => '',
    keywords => '',
    scrap    => 0,
    rating   => 0,
    cat      => 1,
    atype    => 1,
    species  => 1,
    gender   => 0,
};

is $fa->submit->artwork(
       file        => 'logo.png',
       thumb       => 'logo.png',
       title       => 'elaborate',
       description => 'Blablah',
       keywords    => 'white space separated keywords',
       scraps      => 1,
       rating      => 'adult',
       category    => 'Artwork (Digital)',
       theme       => ' aNimA l-relaTed ((NonAnthro  ',
       species     => 'hippopotamus',
       gender      => 'not specified',
   ), 12346, 'elaborate submission';
got_ok {
    title    => 'elaborate',
    message  => 'Blablah',
    keywords => 'white space separated keywords',
    scrap    => 1,
    rating   => 1,
    cat      => 2,
    atype    => 3,
    species  => 6033,
    gender   => 7,
};


for (qw(category theme species gender))
{
    throws_ok { $fa->submit->artwork(%args, $_ => 'nonexistent') }
              qr/Invalid $_ 'nonexistent'/, "invalid $_ dies";
}


throws_ok { $fa->submit->artwork(%args, keywords => 'fail') }
          qr/Unexpected submission result/, 'wrong submission result page dies';


done
