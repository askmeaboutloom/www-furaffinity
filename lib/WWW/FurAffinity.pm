package WWW::FurAffinity;
# ABSTRACT: interact with Fur Affinity [dot] net
use strict;
use warnings;
use overload '""' => sub { shift->{name} };
use Log::Message::Simple;
use Mojo::DOM;
use WWW::Mechanize;


our ($fa, $msg, $debug) = 'https://www.furaffinity.net';


sub new
{
    my $class = shift;
    my %args  = ref $_[0] eq 'HASH' ? %{$_[0]} : @_;

    my $self = bless {
        name => $args{name},
        mech => WWW::Mechanize->new,
    } => $class;

    $self->log_in($args{password}) if exists $args{password};

    $self
}


sub DESTROY
{
    my ($self) = @_;
    $self->log_out if $self->{logged_in};
}


sub dom
{
    my $self = shift;
    my $mech = $self->{mech};
    $self->get unless $mech->res;
    Mojo::DOM->new($mech->content(charset => 'utf-8'))
}


sub get
{
    my $self = shift;
    my $uri  = shift || '';
    $self->{mech}->get("$fa/$uri", @_);
    $self->dom if defined wantarray
}


sub form
{
    my $self   = shift;
    my $fields = ref $_[0] eq 'HASH' ? $_[0] : {@_};
    $self->{mech}->submit_form(with_fields => $fields);
    $self->dom if defined wantarray
}


sub log_in
{
    my ($self, $password) = @_;
    msg "Logging in $self", $msg;
    $self->get('login');
    $self->form(name => $self->{name}, pass => $password);
    die "Couldn't log in" unless $self->logged_in;
    $self
}


sub log_out
{
    my ($self) = @_;
    msg "Logging out $self", $msg;
    $self->get('logout');
    die "Couldn't log out" if $self->logged_in;
    $self
}


sub logged_in
{
    my ($self) = @_;
    my  $dom   = $self->dom;

    if ($dom->at('li.noblock > a[href="/register/"]'))
    {   $self->{logged_in} = '' }
    elsif ($dom->at('#my-username'))
    {   $self->{logged_in} = 1  }
    else
    {   die "Can't tell if $self is logged in or not\n" }
}


sub scrape
{
    require WWW::FurAffinity::Scrape;
    WWW::FurAffinity::Scrape->new(@_)
}


sub submit
{
    require WWW::FurAffinity::Submit;
    WWW::FurAffinity::Submit->new(@_)
}


sub edit
{
    require WWW::FurAffinity::Edit;
    WWW::FurAffinity::Edit->new(@_)
}


sub submission_result
{
    my $uri = shift->{mech}->uri;
    $uri =~ m{/view/(\d+)} or die "Unexpected submission result: $uri\n";
    msg "Submission $1 at $uri", $msg;
    $1
}


1
__END__

=head1 SYNOPSIS

    use WWW::FurAffinity;
    my $fa = WWW::FurAffinity->new(name => 'user', password => 'pass');

    # get your current notifications
    my $messages = $fa->scrape->messages;
    print "$messages->{total} messages:\n";
    for (qw(submissions comments journals favorites watches notes tickets)) {
        print "    $_: $messages->{$_}\n";
    }

    # when $fa goes out of scope, you're logged out automatically

=head1 PUBLIC METHODS

=head2 new

    WWW::FurAffinity->new(name => 'username');
    WWW::FurAffinity->new(name => 'username', password => 'pass');

Constructor for a C<WWW::FurAffinity> object representing the account of the
given user. If a password is given, L</login> is called with it.

=head2 DESTROY

When a C<WWW::FurAffinity> object is destroyed and it thinks that it is still
logged in, C</log_out> is called automatically.

=head2 log_in

    $fa->log_in('password')

Attempts to log in with the username given in the constructor and the given
password. Dies if you aren't logged in afterwards or if it can't tell your
login status at all. Otherwise returns C<$self>.

=head2 log_out

    $fa->log_out

Attempts to log you out. Dies if you aren't logged out afterwards or if it
can't tell your login status at all. Otherwise returns C<$self>.

=head2 logged_in

    $fa->logged_in

Checks if you are logged in according to the current page you are on. Returns
true if you are, false otherwise. Dies if it can't tell which of the two is
the case.

=head1 OPERATIONS

All of these methods return an object that you can call another object on. For
example, to get your current notifications, you'd use
C<< $fa->scrape->messages >>. For documentation about what methods you can call
on them, see the links below.

Be careful with references to these objects, if they last longer than your
C<$fa>, they will prevent it from being destroyed.

=head2 scrape

L<WWW::FurAffinity::Scrape>

=head2 submit

L<WWW::FurAffinity::Submit>

=head1 PRIVATE METHODS

=head2 dom

    $fa->dom;

Returns a L<Mojo::DOM> object of the last request. If there has been no request
yet, it will L</get> the FurAffinity home page first.

=head2 get

    $fa->get;
    $fa->get('page');

GETs the given page of FurAffinity, or the home page if it is not given. For
example C<< $fa->get('login') >> will GET C<https://www.furaffinity.net/login>
and C<< $fa->get >> will GET C<https://www.furaffinity.net/>.

Returns a L<Mojo::DOM> object of the response. In void context, it doesn't
bother to construct that DOM.

This dispatches to L<< WWW::Mechanize/"$mech->get( $uri )" >>, which in turn
dispatches to L<LWP::UserAgent/"REQUEST METHODS">. It will die if the response
does not indicate success.

=head2 form

    $fa->form( %fields)
    $fa->form(\%fields)

Dispatches to L<< WWW::Mechanize/"$mech->submit_form( ... )" >>, using
C<with_fields> and the given fields. Returns a L<Mojo::DOM> object of the
response. In void context, it doesn't bother to construct that DOM. Dies
if the response does not indicate success.
