package WWW::FurAffinity::Edit;
# ABSTRACT: edit things on Fur Affinity [dot] net
use strict;
use warnings;
use Log::Message::Simple;
use WWW::FurAffinity;


my $msg = \$WWW::FurAffinity::msg;


sub new
{
    my ($class, $fa) = @_;
    bless \$fa => $class
}


sub submission
{
    my $fa   = ${shift()};
    my %args = (
        rebuild_thumb => 0,
        ref $_[0] eq 'HASH' ? %{$_[0]} : @_,
    );

    $fa->get("controls/submissions/changesubmission/$args{id}/");
    die "Can't edit submission for $fa: not logged in\n" unless $fa->logged_in;

    $fa->form(
        'newsubmission'     => $args{file},
        'rebuild-thumbnail' => $args{rebuild},
    );

    $fa->submission_result
}


sub info
{
    my $fa   = ${shift()};
    my %args = @_;

    $fa->get("controls/submissions/changeinfo/$args{id}/");
    die "Can't edit submission for $fa: not logged in\n" unless $fa->logged_in;

    # TODO more fields
    my %fields;
    for (qw(title keywords), [description => 'message'])
    {
        my ($arg_key, $field_key) = ref $_ ? @$_ : ($_, $_);
        $fields{$field_key} = $args{$arg_key} if exists $args{$arg_key};
    }
    $fa->form(\%fields);

    $fa->submission_result
}


1
__END__

=head1 PUBLIC METHODS

=head1 PRIVATE METHODS

=head2 new

    WWW::FurAffinity::Edit->new(WWW::FurAffinity $fa);

Constructs a new object. Don't call this directly, use
L<WWW::FurAffinity/edit>.
