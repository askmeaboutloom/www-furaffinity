package WWW::FurAffinity::Submit;
# ABSTRACT: post things to Fur Affinity [dot] net
use strict;
use warnings;
use Log::Message::Simple;
use WWW::FurAffinity;


my $msg = \$WWW::FurAffinity::msg;


sub new
{
    my ($class, $fa) = @_;
    bless \$fa => $class
}


sub normalize
{
    (my $str = lc shift) =~ s/[^0-9a-z]//g;
    $str
}


sub extract_options
{
    my ($dom) = @_;
    my %options;

    $dom->find('input[name="rating"]')->each(sub
    {
        my $key = normalize($_->parent->at('strong')->content);
        $options{rating}{$key} = $_->attr('value')
    });

    for my $name (qw(cat atype species gender))
    {
        $dom->find(qq(select[name="$name"] option))->each(sub
        {
            for my $key ($_->text, split '/', $_->text)
            {   $options{$name}{normalize($key)} = $_->attr('value') }
        });
    }

    \%options
}


sub submission_fields
{
    my ($dom, $args) = @_;

    my %fields = (
        title    => $args->{title},
        message  => $args->{description} || '',
        keywords => $args->{keywords}    || '',
        scrap    => $args->{scraps} ? 1 : 0,
    );

    my $opts = extract_options($dom);
    for ([category => 'cat'], [theme => 'atype'], qw(species gender rating))
    {
        my ($arg_key, $opt_key) = ref $_ ? @$_ : ($_, $_);
        my $arg = $args->{$arg_key} or next;
        my $opt = $opts->{$opt_key}{normalize($arg)};
        if (!defined $opt)
        {
            my @keys = map { " - $_\n" } sort keys %{$opts->{$opt_key}};
            die "Invalid $arg_key '$arg', must be one of:\n", @keys;
        }
        $fields{$opt_key} = $opt;
    }

    \%fields
}


sub artwork
{
    my $fa   = ${shift()};
    my %args = ref $_[0] eq 'HASH' ? %{$_[0]} : @_;

    for (qw(file title rating))
    {   die "missing required argument '$_'\n" unless length $args{$_} }

    for (qw(file thumb))
    {   die "not a file: '$args{$_}'" if $args{$_} && !-f $args{$_} }

    $fa->get('submit/');
    die "Can't submit artwork for $fa: not logged in\n" unless $fa->logged_in;

    msg "Submitting artwork for $fa (1/3 starting)", $$msg;
    $fa->form(submission_type => 'submission');

    msg "Submitting artwork for $fa (2/3 uploading)", $$msg;
    $fa->form(
                        submission => $args{file },
        $args{thumb} ? (thumbnail  => $args{thumb}) : (),
    );

    msg "Submitting artwork for $fa (3/3 finalizing)", $$msg;
    $fa->form(submission_fields($fa->dom, \%args));

    $fa->submission_result
}


1
__END__

=head1 PUBLIC METHODS

=head1 PRIVATE METHODS

=head2 new

    WWW::FurAffinity::Submit->new(WWW::FurAffinity $fa);

Constructs a new object. Don't call this directly, use
L<WWW::FurAffinity/submit>.
