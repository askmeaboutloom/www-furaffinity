package WWW::FurAffinity::Scrape;
# ABSTRACT: scrape information from Fur Affinity [dot] net
use strict;
use warnings;
use Time::Piece;
use WWW::FurAffinity;


sub new
{
    my ($class, $fa) = @_;
    bless \$fa => $class
}


sub options
{
    my $fa  = ${shift()};
    my $dom = $fa->get('browse/');
    my %options;

    for ([category => 'cat'], [theme => 'atype'], qw(species gender))
    {
        my ($key, $name) = ref $_ ? @$_ : ($_, $_);
        $options{$key} = $dom->find(qq(select[name="$name"] option))
                             ->map(sub { $_->text })
                             ->to_array;
    }

    \%options
}


sub date
{
    my $date;

    if (shift =~ m{
                (?<month>\w{3})\w* # abbreviated month
            \s+
                (?<day>\d+)
            \w+,\s+                # st/nd/rd/th and a comma
                (?<year>\d+)
            (,)?\s+                # some dates have a comma here
                (?<time>\d+:\d+)
            \s+
                (?<ampm>\w+)
        }x)
    {
        my $str = "$+{year} $+{month} $+{day} $+{time} $+{ampm}";
        eval { $date = Time::Piece->strptime($str, '%Y %b %d %I:%M %p') };
        warn $@ if $@;
    }

    $date
}


sub messages
{
    my $fa = ${shift()};
    $fa->get;
    die "Can't get messages for $fa: not logged in\n" unless $fa->logged_in;

    my $html = $fa->dom->at('.dropdown-left > .noblock')->all_text;
    my %messages;

    for (qw(submissions comments journals favorites watches notes tickets))
    {
        my $letter = uc substr $_, 0, 1;
        my $count  = $html =~ /(\d+)$letter/ ? $1 : 0;
        $messages{ $_  }  = $count;
        $messages{total} += $count;
    }

    \%messages
}


sub submission
{
    my $fa = ${shift()};
    my $id = shift or die "Missing submission ID\n";
    $id = $1 if $id =~ m{/(?:view|full)/(\d+)};

    my $dom  = $fa->get("view/$id");
    my $info = $dom->at('td[valign="top"][align="left"].alt1');
    my $text = $info->all_text;

    my %data = (
        title       => $dom->at('tr > td > b')->text,
        artist      => $dom->at('tr > td > a')->text,
        artist_link => $dom->at('tr > td > a')->attr('href'),
        date        => date($info->at('span')),

        download    => $dom->find('.alt1.actions a')
                           ->first(sub{ $_->text eq 'Download' })
                           ->attr('href'),

        keywords    => $dom->find('#keywords a')
                            ->map(sub { $_->text })
                            ->to_array,
    );

    if ($info->at('img')->attr('alt') =~ /(\S+)/)
    {   $data{rating} = lc $1 }

    for (qw(favorites comments views))
    {   $data{$_} = $1 if $text =~ /$_:\s+(\d+)/i }

    for (qw(category theme species gender resolution))
    {   $data{$_} = $1 if $text =~ /$_:\s+([^:]+)(\s+\w+|\s*$)/i }

    # TODO comments maybe?
    # TODO handle different submission types

    \%data
}


sub info
{
    my $fa = ${shift()};
    my $id = shift or die "Missing submission ID\n";
    $id = $1 if $id =~ m{/(?:view|full)/(\d+)};

    my $dom = $fa->get("controls/submissions/changeinfo/$id/");

    # TODO more info
    {
        title       => $dom->at('input[name="title"]')->attr('value'),
        description => $dom->at('textarea[name="message"]')->text,
        keywords    => [split ' ', $dom->at('textarea[name="keywords"]')->text],
    }
}


sub notes
{
    my $fa     = ${shift()};
    my $page   = shift || 1;
    my $folder = shift;

    $fa->get("controls/switchbox/$folder") if $folder;
    $fa->get("msg/pms/$page");
    die "Can't get notes for $fa: not logged in\n" unless $fa->logged_in;

    my $notes = $fa->dom->find('.note')->map(sub
    {
        my $notelink = $_->at('.notelink');
        my $user     = $_->at('td.nowrap > a');
        my $class    = $notelink->attr('class');

        {
            id        => $_->at('input')->attr('value'),
            link      => $notelink->attr('href'),
            subject   => $notelink->text,
            user      => $user->text,
            user_link => $user->attr('href'),
            read      => $class =~ /\bnote-read\b/,
            priority  => $class =~ /\b(\w+)_prio\b/ ? $1 : undef,
            date      => date($_->at('span')),
        }
    });

    $notes->to_array
}


sub note
{
    my $fa = ${shift()};
    my $id = shift or die "Missing note ID\n";
    $id = $1 if $id =~ m{/viewmessage/(\d+)};

    $fa->get("viewmessage/$id");
    die "Can't get note $id for $fa: not logged in\n" unless $fa->logged_in;

    my $note = $fa->dom->at('table.maintable td.alt1')
        or die "Can't get note $id for $fa: doesn't exist or not yours\n";
    $note->content
}


1
__END__

=head1 PUBLIC METHODS

=head2 messages

    $fa->scrape->messages;

GETs the FurAffinity home page and gathers the message counts from the
notification area at the top right. Returns a hashref containing the message
counts for the following keys:

=for :list
* submissions
* comments
* journals
* favorites
* watches
* notes
* tickets
* total

Dies if you're not logged in or if it can't tell if you're logged in.

=head1 PRIVATE METHODS

=head2 new

    WWW::FurAffinity::Scrape->new(WWW::FurAffinity $fa);

Constructs a new object. Don't call this directly, use
L<WWW::FurAffinity/scrape>.
