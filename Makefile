test:
	prove -l lib t

cover:
	cover -test -ignore_re '\.(t|pl)$$'

install:
	dzil install

clean:
	rm -rf .build cover_db WWW-FurAffinity-*
